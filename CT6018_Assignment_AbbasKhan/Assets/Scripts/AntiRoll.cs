﻿///////////////////////////////////////////////////////////////////
// Name: AntiRoll.cs
// Author: Abbas Khan
// Date: 23/09/18
// Bio: Used to control the AntiRoll of the vehicle
///////////////////////////////////////////////////////////////////

// Using Calls
using UnityEngine;

public class AntiRoll : MonoBehaviour
{
    // Variables to hold the wheel collider for the left and right side
    public WheelCollider xWheelLeft;
    public WheelCollider xWheelRight;
    // Float for the power multiplier of the anti roll force
    public float fAntiRollAmount = 5000.0f;
    // Rigidbody to hold the local rigidbody
    private Rigidbody xLocalRigidbody;
    // WheelHit variable used to hold the wheelcollider contact point
    private WheelHit xHit;
    // Floats used to hold the distance shifted by the wheel
    private float fTravelLeft = 1.0f;
    private float fTravelRight = 1.0f;
    // Float used to hold the total power of the anti roll force to be applied
    private float antiRollForce;
    // Booleans used to tell if the wheel is grounded
    private bool bGroundedRight;
    private bool bGroundedLeft;

    private void Start()
    {
        // Retrieve the local rigidbody component
        xLocalRigidbody = gameObject.GetComponent<Rigidbody>();
    }

    public void FixedUpdate()
    {
        // Safety null check if variables not assigned
        if (xWheelLeft != null && xWheelRight != null)
        {
            // Determine whether the wheel is grounded
            bGroundedLeft = xWheelLeft.GetGroundHit(out xHit);
            if (bGroundedLeft == true)
            {
                fTravelLeft = (-xWheelLeft.transform.InverseTransformPoint(xHit.point).y - xWheelLeft.radius) / xWheelLeft.suspensionDistance;
            }

            // Determine whether the wheel is grounded
            bGroundedRight = xWheelRight.GetGroundHit(out xHit);
            if (bGroundedRight == true)
            {
                fTravelRight = (-xWheelRight.transform.InverseTransformPoint(xHit.point).y - xWheelRight.radius) / xWheelRight.suspensionDistance;
            }

            // Calculate the total power of the anti roll force to be applied
            antiRollForce = (fTravelLeft - fTravelRight) * fAntiRollAmount;

            if (bGroundedLeft == true)
            {
                // Safety null check
                if (xLocalRigidbody != null)
                {
                    // Add the force to a specific side of the rigidbody to counteract the tilt
                    xLocalRigidbody.AddForceAtPosition(xWheelLeft.transform.up * -antiRollForce, xWheelLeft.transform.position);
                }
            }
            if (bGroundedRight == true)
            {
                // Safety null check
                if (xLocalRigidbody != null)
                {
                    // Add the force to a specific side of the rigidbody to counteract the tilt
                    xLocalRigidbody.AddForceAtPosition(xWheelRight.transform.up * antiRollForce, xWheelRight.transform.position);
                }
            }
        }
    }
}