﻿///////////////////////////////////////////////////////////////////
// Name: CameraOrbit.cs
// Author: Abbas Khan
// Date: 22/09/18
// Bio: Used to control the camera
///////////////////////////////////////////////////////////////////

// Using Calls
using UnityEngine;

public class CameraOrbit : MonoBehaviour
{
    // Constant floats used to hold the maximum angles for camera rotation in X
    private const float X_ANGLE_MIN = 0.0f;
    private const float X_ANGLE_MAX = 50.0f;
    // Transform to hold the target object for orbiting around
    public Transform xTargetTransform;
    // Transform to hold local camera's transform
    private Transform xCamTransform;
    // Float for the distance between the orbit point and the camera itself (Orbit point is pivot of object)
    public float fDistance = 8.0f;
    // Floats used to hold the current X and Y values 
    private float fCurrentX = 0.0f;
    private float fCurrentY = 45.0f;
    // Vector3 to hold the distance offset
    private Vector3 xDirection;
    // Quaternion to hold the rotation from the input
    private Quaternion xRotation;

    void Start()
    {
        // Assignment to the local transform
        xCamTransform = transform;
    }

    void Update()
    {
        // Get the input from the mouse for the X and Y
        fCurrentX += Input.GetAxis("Mouse X");
        fCurrentY -= Input.GetAxis("Mouse Y");
        // Clamp the Y to be between the two constants
        fCurrentY = Mathf.Clamp(fCurrentY, X_ANGLE_MIN, X_ANGLE_MAX);
    }

    void LateUpdate()
    {
        // Hold the distance offset
        xDirection = new Vector3(0, 0, -fDistance);
        // Assign the rotation from the input
        xRotation = Quaternion.Euler(fCurrentY, fCurrentX, 0);
        // Assign both to the camera
        xCamTransform.position = xTargetTransform.position + xRotation * xDirection;
        // Assign the target to look at
        xCamTransform.LookAt(xTargetTransform.position);
    }
}
