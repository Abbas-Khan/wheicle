﻿///////////////////////////////////////////////////////////////////
// Name: PluginManager.cs
// Author: Abbas Khan
// Date: 21/09/18
// Bio: Used to control the plugin
///////////////////////////////////////////////////////////////////

// Using Calls
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PluginManager : EditorWindow {

    // GUI Variables
    float fWidth;
    float fHeight = 30;
    string[] sActionLabels = new string[] { "Welcome", "Rig Vehicle", "Information" };
    // Vector2's for scrolling the GUI
    private Vector2 xUIScroll;
    private Vector2 xUIScroll2;
    // Texture 2D to hold the Wheicle Logo
    private Texture2D xLogo;
    // Integer used to switch between the GUI tabs
    public static int iselectedAction = 0;
    // Booleans used to open segments of the UI
    public bool bCreateNewHub = false;
    public bool bConfigureCamera = false;
    public bool bConfigureModel = false;
    public bool bConfigureHub = false;
    public bool bConfigureSetup = false;
    public bool bConfigureVehicle = false;
    // Gameobject yo hold the camera
    public static GameObject xCamera;
    // Strings to hold the model and hub names
    public static string sModelName = "Insert Text Here";
    public static string sHubName = "Insert Text Here";
    // Gameobjects to hold the wheel prefabs for either side
    public static GameObject xWheelPrefab_L;
    public static GameObject xWheelPrefab_R;
    // Gameobject arrays to hold the attatchment points
    public static GameObject[] xExhaust_Attatchments = new GameObject[10];
    public static GameObject[] xHeadlights_Attatchments = new GameObject[10];
    public static GameObject[] xBrakelights_Attatchments = new GameObject[10];
    public static GameObject[] xReverselights_Attatchments = new GameObject[10];
    // Booleans used to add components to the setup
    public static bool bAddExhaust = true;
    public static bool bAddBoost = false;
    public static bool bAddHeadlights = true;
    public static bool bAddReverselights = true;
    public static bool bAddBrakelights = true;
    public static bool bAddSkidTrails = true;
    public static bool bAddWheels = false;
    // Integers used to specify the amount of components to add to the setup
    public static int iAmountOfExhaustandBoostParticles = 0;
    public static int iAmountOfLights_Head = 0;
    public static int iAmountOfLights_Reverse = 0;
    public static int iAmountOfLights_Brake = 0;
    public static int iAmountOfSkidTrails = 0;
    public static int iAmountOfWheels = 0;
    // Variables to determine the setup of the Vehicle
    public static bool bIsManual = false;
    public static int iDriveType = 0;
    public static int iBHP = 500;
    public static int iBrakeTorque = 250;
    public static int iMaxSpeed = 350;
    public static int iMaxReverseSpeed = -40;
    public static int iSteerAngleLowSpeed = 13;
    public static int iSteerAngleHighSpeed = 2;
    public static int iSteerAngleHighSpeedThreshold = 100;

    // Function to open the GUi with tab 0 open
    [MenuItem("Window/Wheicle - Vehicle Rigger/Welcome")]
    public static void Welcome()
    {
        // Create a new window
        EditorWindow window = EditorWindow.GetWindow(typeof(PluginManager), false, "Vehicle Rigger", true);
        // Size specifications
        window.maxSize = new Vector2(444, 400);
        window.minSize = window.maxSize;
        // Tab 0 to be open
        iselectedAction = 0;
    }

    // Function to open the GUi with tab 1 open
    [MenuItem("Window/Wheicle - Vehicle Rigger/Rig Vehicle")]
    public static void RigVehicle()
    {
        // Create a new window
        EditorWindow window = EditorWindow.GetWindow(typeof(PluginManager), false, "Vehicle Rigger", true);
        // Size specifications
        window.maxSize = new Vector2(444, 400);
        window.minSize = window.maxSize;
        // Tab 1 to be open
        iselectedAction = 1;
    }

    // Function to open the GUi with tab 2 open
    [MenuItem("Window/Wheicle - Vehicle Rigger/Information")]
    public static void Information()
    {
        // Create a new window
        EditorWindow window = EditorWindow.GetWindow(typeof(PluginManager), false, "Vehicle Rigger", true);
        // Size specifications
        window.maxSize = new Vector2(444, 400);
        window.minSize = window.maxSize;
        // Tab 2 to be open
        iselectedAction = 2;
    }

    // Function to create a new hub takes a string arguement for the name
    private static void CreateHub(string a_name)
    {
        // Length safety check
        if(a_name.Length != 0)
        {
            // Create a new object with the specified name
            GameObject THEHUB = new GameObject(a_name);
            Undo.RegisterCreatedObjectUndo(THEHUB, "HUB Creation");
            THEHUB.transform.position = new Vector3(0, 0, 0);
            // Bebug.log to provide response to user
            Debug.Log("Hub created");
        }
        else
        {
            // Bebug.log to provide response to user
            Debug.Log("Invalid name");
        }
    }

    // Function to setup a camera takes a string arguement for the name
    private static void SetupCamera(string a_name)
    {
        // Find the object with the parameter name
        GameObject obj = GameObject.Find(a_name);
        // Safety null check
        if (obj != null)
        {
            Undo.SetCurrentGroupName("Camera Setup");
            // Add the camera script and set the default values
            Undo.AddComponent<CameraOrbit>(obj);
            CameraOrbit CO = obj.GetComponent<CameraOrbit>();
            CO.fDistance = 10;
            CO.xTargetTransform = GameObject.Find(sHubName).transform;
            // Bebug.log to provide response to user
            Debug.Log("Camera configured");
        }
        else
        {
            // Bebug.log to provide response to user
            Debug.Log("No camera found with given name. Please enter a valid name.");
        }
    }

    // Function to setup a rigidbody tales a string arguement for the name
    private static void SetupMainHubRigidBody(string a_name)
    {
        // Find the object with the parameter name
        GameObject obj = GameObject.Find(a_name);
        // Safety null check
        if (obj != null)
        {
            Undo.SetCurrentGroupName("Vehicle Rigidbody Setup");
            // Add the rigidbody and set the default values
            Undo.AddComponent<Rigidbody>(obj);
            Rigidbody RB = obj.GetComponent<Rigidbody>();
            RB.mass = 1500;
            RB.drag = 0.05f;
            // Bebug.log to provide response to user
            Debug.Log("Rigidbody added");
        }
        else
        {
            // Bebug.log to provide response to user
            Debug.Log("No hub found with given name. Please enter a valid name.");
        }
    }

    // Function to setup a hub tales a string arguement for the name
    private static void SetupMainHub(string a_name)
    {
        // Find the object with the parameter name
        GameObject obj = GameObject.Find(a_name);
        // Safety null check
        if (obj != null)
        {
            Undo.SetCurrentGroupName("Vehicle Scripts Setup");
            // Add the scripts to the object
            Undo.AddComponent<VehicleController>(obj);
            Undo.AddComponent<Suspension>(obj);
            Undo.AddComponent<Wing>(obj);
            // Safety 0 check
            if(iAmountOfWheels != 0)
            {
                // Add the amount of scripts
                for(int i = 0; i < iAmountOfWheels / 2; i++)
                {
                    Undo.AddComponent<AntiRoll>(obj);
                }
            }
            // If add skid trails is true
            if(bAddSkidTrails == true)
            {
                // Add the amount of scripts
                for (int i = 0; i < iAmountOfSkidTrails; i++)
                {
                    Undo.AddComponent<SkidTrails>(obj);
                }
            }
            // Bebug.log to provide response to user
            Debug.Log("Hub configured");
        }
        else
        {
            // Bebug.log to provide response to user
            Debug.Log("No hub found with given name. Please enter a valid name.");
        }
    }

    // Function to setup wheel colliders tales a string arguement for the name
    private static void AddWheelColliders(string a_name)
    {
        // Find the object with the parameter name
        GameObject obj = GameObject.Find(a_name);
        // Safety null check
        if (obj != null)
        {
            // Create a wheel colliders parent object
            GameObject WheelColliders = new GameObject("WheelColliders");
            Undo.RegisterCreatedObjectUndo(WheelColliders, "WheelColliders Creation");
            WheelColliders.transform.position = new Vector3(0, 0, 0);
            WheelColliders.transform.SetParent(obj.transform, false);
            // Template objects for the various components
            #region Templates
            JointSpring suspensionSpring = new JointSpring();
            suspensionSpring.spring = 35000;
            suspensionSpring.damper = 1500;
            suspensionSpring.targetPosition = 0.5F;

            WheelFrictionCurve WFC = new WheelFrictionCurve();
            WFC.extremumSlip = 0.4f;
            WFC.extremumValue = 1;
            WFC.asymptoteSlip = 0.8f;
            WFC.asymptoteValue = 0.5f;
            WFC.stiffness = 0.9f;

            WheelFrictionCurve WFC2 = new WheelFrictionCurve();
            WFC2.extremumSlip = 0.2f;
            WFC2.extremumValue = 1;
            WFC2.asymptoteSlip = 0.5f;
            WFC2.asymptoteValue = 0.75f;
            WFC2.stiffness = 0.8f;
            #endregion
            // Loop through the amount of wheels, create a new wheel collider and assin the properties
            for (int i = 0; i < iAmountOfWheels; i++)
            {
                GameObject WheelCollider = new GameObject("WheelCollider" + "_" + i);
                Undo.RegisterCreatedObjectUndo(WheelCollider, WheelCollider.name + " Creation");
                WheelCollider.transform.position = new Vector3(0, 0, 0);
                WheelCollider.transform.SetParent(WheelColliders.transform, false);
                WheelCollider.AddComponent<WheelCollider>();
                WheelCollider WC = WheelCollider.GetComponent<WheelCollider>();
                WC.mass = 20;
                WC.wheelDampingRate = 0.25f;
                WC.forceAppPointDistance = 0;
                WC.suspensionSpring = suspensionSpring;
                WC.forwardFriction = WFC;
                WC.sidewaysFriction = WFC2;
            }
            // Bebug.log to provide response to user
            Debug.Log("Wheel Colliders Added");
        }
        else
        {
            // Bebug.log to provide response to user
            Debug.Log("No hub found with given name. Please enter a valid name.");
        }
    }

    // Function to setup a box collider tales a string arguement for the name
    private static void SetupMainCollider(string a_name)
    {
        // Find the object with the parameter name
        GameObject obj = GameObject.Find(a_name);
        // Safety null check
        if (obj != null)
        {
            Undo.SetCurrentGroupName("Vehicle Collider Setup");
            // Add a box collider component
            Undo.AddComponent<BoxCollider>(obj);
            // Bebug.log to provide response to user
            Debug.Log("Collider added to " + obj.name);
        }
        else
        {
            // Bebug.log to provide response to user
            Debug.Log("No model found with given name. Please enter a valid name.");
        }
    }

    // Function to add the model components tales a string arguement for the name
    private static void AddComponentsModel(string a_name)
    {
        // Find the object with the parameter name
        GameObject obj = GameObject.Find(a_name);
        // Safety null check
        if (obj != null)
        {
            // Wheel model parent object creation
            #region Wheel Parents
            // Create a parent object for all the wheels
            GameObject Wheels = new GameObject("Wheels");
            Undo.RegisterCreatedObjectUndo(Wheels, "Wheels Creation");
            Wheels.transform.position = new Vector3(0, 0, 0);
            Wheels.transform.SetParent(obj.transform, false);

            // Loop through and create a local parent for each wheel
            for(int i = 0; i < iAmountOfWheels; i++)
            {
                GameObject WheelParent = new GameObject("WheelParent" + "_" + i);
                Undo.RegisterCreatedObjectUndo(WheelParent, WheelParent.name + " Creation");
                WheelParent.transform.position = new Vector3(0, 0, 0);
                WheelParent.transform.SetParent(Wheels.transform, false);
            }
            #endregion

            // Exhaust object creation
            if (bAddExhaust == true)
            {
                // Loop through the amount of exhaust objects
                for (int i = 0; i < iAmountOfExhaustandBoostParticles; i++)
                {
                    // Create an exhaust object
                    GameObject Exhaust_Object;
                    // If there is no attachment
                    if(xExhaust_Attatchments[i] == null)
                    {
                        // Create a primitive object to serve as an exhaust pipe and setup the properties
                        Exhaust_Object = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
                        Exhaust_Object.name = "Exhaust_Object" + "_" + i;
                        Undo.RegisterCreatedObjectUndo(Exhaust_Object, Exhaust_Object.name + " Creation");
                        Exhaust_Object.transform.position = new Vector3(0, 0, 0);
                        Exhaust_Object.transform.SetParent(obj.transform, false);
                        DestroyImmediate(Exhaust_Object.GetComponent<CapsuleCollider>());
                        Exhaust_Object.transform.Rotate(90f, 0, 0);
                        Exhaust_Object.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
                    }
                    else
                    {
                        // Otherwise, if there is one assign it to this and change it's name
                        Exhaust_Object = xExhaust_Attatchments[i];
                        Exhaust_Object.name = "Exhaust_Object" + "_" + i;
                    }

                    // Create an exhaust particle system, attatch it to the exhaust pbject and setup the properties
                    GameObject Exhaust_ParticleSystem = new GameObject("Exhaust_PL_" + i);
                    Undo.RegisterCreatedObjectUndo(Exhaust_ParticleSystem, Exhaust_ParticleSystem.name + " Creation");
                    Exhaust_ParticleSystem.transform.position = new Vector3(0, 0, 0);
                    Exhaust_ParticleSystem.transform.SetParent(Exhaust_Object.transform, false);
                    Exhaust_ParticleSystem.AddComponent<ParticleSystem>();
                    ParticleSystem EPL_Component = Exhaust_ParticleSystem.GetComponent<ParticleSystem>();
                    ParticleSystem.MainModule main_module = EPL_Component.main;
                    main_module.startSpeed = 0.5f;
                    main_module.startSize = 0.4f;
                    main_module.maxParticles = 10;
                    main_module.startLifetime = 1;
                    ParticleSystem.ShapeModule shape_module = EPL_Component.shape;
                    shape_module.angle = 1.41f;
                    shape_module.radius = 0.0001f;
                    ParticleSystem.SizeOverLifetimeModule size_module = EPL_Component.sizeOverLifetime;
                    size_module.enabled = true;
                    size_module.size = new ParticleSystem.MinMaxCurve(1.0f, AnimationCurve.Linear(0, 1, 1, 0.2f));
                    ParticleSystemRenderer PS_Renderer = EPL_Component.GetComponent<ParticleSystemRenderer>();
                    PS_Renderer.material = new Material(Shader.Find("Particles/Standard Unlit"));

                    // If add boost is true
                    if (bAddBoost == true)
                    {
                        // Create a boost particle system, attatch it to the exhaust object and setup the properties
                        GameObject Boost_PL = new GameObject("Boost_PL_" + i);
                        Undo.RegisterCreatedObjectUndo(Boost_PL, Boost_PL.name + " Creation");
                        Boost_PL.transform.position = new Vector3(0, 0, 0);
                        Boost_PL.transform.SetParent(Exhaust_Object.transform, false);
                        Boost_PL.AddComponent<ParticleSystem>();
                        ParticleSystem BPL = Boost_PL.GetComponent<ParticleSystem>();
                        ParticleSystem.MainModule main_B = BPL.main;
                        main_B.startSpeed = 0.8f;
                        main_B.startColor = new Color(0, 0.84f, 1, 1);
                        main_B.startSize = 0.4f;
                        main_B.maxParticles = 50;
                        main_B.startLifetime = 1;
                        ParticleSystem.EmissionModule EB = BPL.emission;
                        EB.rateOverTime = 40;

                        // Create a gradient for the particle system
                        Gradient g;
                        GradientColorKey[] gck;
                        GradientAlphaKey[] gak;
                        g = new Gradient();
                        gck = new GradientColorKey[2];
                        gck[0].color = new Color(0, 0.84f, 1, 1);
                        gck[0].time = 0.0F;
                        gck[1].color = Color.white;
                        gck[1].time = 1.0F;
                        gak = new GradientAlphaKey[2];
                        gak[0].alpha = 1.0F;
                        gak[0].time = 0.0F;
                        gak[1].alpha = 1.0f;
                        gak[1].time = 1.0F;
                        g.SetKeys(gck, gak);

                        // Apply a gradient for the particle system
                        ParticleSystem.ColorOverLifetimeModule CB = BPL.colorOverLifetime;
                        CB.enabled = true;
                        CB.color = new ParticleSystem.MinMaxGradient(g);
                        ParticleSystem.ShapeModule shape_B = BPL.shape;
                        shape_B.angle = 1.41f;
                        shape_B.radius = 0.0001f;
                        ParticleSystem.SizeOverLifetimeModule size_B = BPL.sizeOverLifetime;
                        size_B.enabled = true;
                        size_B.size = new ParticleSystem.MinMaxCurve(1.0f, AnimationCurve.Linear(0, 1, 1, 0.2f));
                        ParticleSystemRenderer PSR_B = BPL.GetComponent<ParticleSystemRenderer>();
                        PSR_B.material = new Material(Shader.Find("Particles/Standard Unlit"));
                    }
                }
            }

            // Wheel object creation
            if (bAddWheels == true)
            {
                // Loop through the amount of wheel
                for(int i = 0; i < iAmountOfWheels; i++)
                {
                    // If it's even
                    if(i%2 == 0)
                    {
                        // Create a left sided wheel and attach it to the parent
                        GameObject WheelL = (GameObject)PrefabUtility.InstantiatePrefab(xWheelPrefab_L);
                        WheelL.name = "WheelL" + "_" + i;
                        Undo.RegisterCreatedObjectUndo(WheelL, WheelL.name + " Creation");
                        WheelL.transform.position = new Vector3(0, 0, 0);
                        WheelL.transform.SetParent(GameObject.Find("WheelParent" + "_" + i).transform, false);
                    }
                    else
                    {
                        // Create a right sided wheel and attach it to the parent
                        GameObject WheelR = (GameObject)PrefabUtility.InstantiatePrefab(xWheelPrefab_R);
                        WheelR.name = "WheelR" + "_" + i;
                        Undo.RegisterCreatedObjectUndo(WheelR, WheelR.name + " Creation");
                        WheelR.transform.position = new Vector3(0, 0, 0);
                        WheelR.transform.SetParent(GameObject.Find("WheelParent" + "_" + i).transform, false);
                    }
                }
            }

            // Head light creation
            if(bAddHeadlights == true)
            {
                // Create a parent object for all the front lights
                GameObject FrontLights = new GameObject("FrontLights");
                Undo.RegisterCreatedObjectUndo(FrontLights, "FrontLights Creation");
                FrontLights.transform.position = new Vector3(0, 0, 0);
                FrontLights.transform.SetParent(obj.transform, false);

                // Loop through the amount of head light objects
                for (int i = 0; i < iAmountOfLights_Head; i++)
                {
                    // Create a light container object
                    GameObject SL_Container;
                    // If there is no attachment
                    if (xHeadlights_Attatchments[i] == null)
                    {
                        // Create a primitive object to serve as a head light and setup the properties
                        SL_Container = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
                        SL_Container.name = "SL_Container" + "_" + i;
                        Undo.RegisterCreatedObjectUndo(SL_Container, SL_Container.name + " Creation");
                        SL_Container.transform.position = new Vector3(0, 0, 0);
                        SL_Container.transform.SetParent(FrontLights.transform, false);
                        DestroyImmediate(SL_Container.GetComponent<CapsuleCollider>());
                        SL_Container.transform.Rotate(90f, 0, 0);
                        SL_Container.transform.localScale = new Vector3(0.1f, 0.01f, 0.2f);
                    }
                    else
                    {
                        // Otherwise, if there is one assign it to this, set the parent and change it's name
                        SL_Container = xHeadlights_Attatchments[i];
                        SL_Container.name = "SL_Container" + "_" + i;
                        SL_Container.transform.SetParent(FrontLights.transform, false);
                    }

                    // Create a light object, parent it to the container and setup the properties
                    GameObject SL = new GameObject("Spotlight" + "_" + i);
                    Undo.RegisterCreatedObjectUndo(SL, SL.name + " Creation");
                    SL.transform.position = new Vector3(0, 0, 0);
                    SL.transform.SetParent(SL_Container.transform, false);
                    SL.transform.Rotate(-90f, 0, 0);
                    SL.AddComponent<Light>();
                    Light L1 = SL.GetComponent<Light>();
                    L1.type = LightType.Spot;
                    L1.range = 100;
                    L1.intensity = 4;
                    L1.spotAngle = 35;
                    Color LightColor = new Color(1f, 0.95f, 0.84f, 1f);
                    L1.color = LightColor;
                    L1.shadows = LightShadows.Soft;
                    L1.shadowBias = 0.02f;
                    L1.shadowNormalBias = 0.1f;
                    L1.shadowNearPlane = 0.1f;
                }
            }

            // Reverse light creation
            if (bAddReverselights == true)
            {
                // Create a parent object for all the reverse lights
                GameObject ReverseLights = new GameObject("ReverseLights");
                Undo.RegisterCreatedObjectUndo(ReverseLights, "ReverseLights Creation");
                ReverseLights.transform.position = new Vector3(0, 0, 0);
                ReverseLights.transform.SetParent(obj.transform, false);

                // Loop through the amount of reverse light objects
                for (int i = 0; i < iAmountOfLights_Reverse; i++)
                {
                    // Create a light container object
                    GameObject RL_Container;
                    // If there is no attachment
                    if (xReverselights_Attatchments[i] == null)
                    {
                        // Create a primitive object to serve as a reverse light and setup the properties
                        RL_Container = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
                        RL_Container.name = "RL_Container" + "_" + i; ;
                        Undo.RegisterCreatedObjectUndo(RL_Container, RL_Container.name + " Creation");
                        RL_Container.transform.position = new Vector3(0, 0, 0);
                        RL_Container.transform.SetParent(ReverseLights.transform, false);
                        DestroyImmediate(RL_Container.GetComponent<CapsuleCollider>());
                        RL_Container.transform.Rotate(90f, 0, 0);
                        RL_Container.transform.localScale = new Vector3(0.1f, 0.01f, 0.2f);
                    }
                    else
                    {
                        // Otherwise, if there is one assign it to this, set the parent and change it's name
                        RL_Container = xReverselights_Attatchments[i];
                        RL_Container.name = "RL_Container" + "_" + i;
                        RL_Container.transform.SetParent(ReverseLights.transform, false);
                    }
                }
            }

            // Brake light creation
            if (bAddBrakelights == true)
            {
                // Create a parent object for all the brake lights
                GameObject BrakeLights = new GameObject("BrakeLights");
                Undo.RegisterCreatedObjectUndo(BrakeLights, "BrakeLights Creation");
                BrakeLights.transform.position = new Vector3(0, 0, 0);
                BrakeLights.transform.SetParent(obj.transform, false);

                // Loop through the amount of brake light objects
                for (int i = 0; i < iAmountOfLights_Brake; i++)
                {
                    // Create a light container object
                    GameObject BL_Container;
                    // If there is no attachment
                    if (xBrakelights_Attatchments[i] == null)
                    {
                        // Create a primitive object to serve as a brake light and setup the properties
                        BL_Container = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
                        BL_Container.name = "BL_Container" + "_" + i;
                        Undo.RegisterCreatedObjectUndo(BL_Container, BL_Container.name + " Creation");
                        BL_Container.transform.position = new Vector3(0, 0, 0);
                        BL_Container.transform.SetParent(BrakeLights.transform, false);
                        DestroyImmediate(BL_Container.GetComponent<CapsuleCollider>());
                        BL_Container.transform.Rotate(90f, 0, 0);
                        BL_Container.transform.localScale = new Vector3(0.1f, 0.01f, 0.2f);
                    }
                    else
                    {
                        // Otherwise, if there is one assign it to this, set the parent and change it's name
                        BL_Container = xBrakelights_Attatchments[i];
                        BL_Container.name = "BL_Container" + "_" + i;
                        BL_Container.transform.SetParent(BrakeLights.transform, false);
                    }
                }
            }

            // Skid trail creation
            if(bAddSkidTrails == true)
            {
                // Loop through the amount of skid trails
                for (int i = 0; i < iAmountOfSkidTrails; i++)
                {
                    // Create a skid trail, parent it to a wheel and set the properties
                    GameObject GO = new GameObject("SkidTrail" + "_" + i);
                    Undo.RegisterCreatedObjectUndo(GO, "Trail Creation");
                    GO.transform.position = new Vector3(0, 0, 0);
                    GO.transform.SetParent(GameObject.Find("WheelCollider" + "_" + i).transform, false);
                    GO.AddComponent<TrailRenderer>();
                    TrailRenderer TR = GO.GetComponent<TrailRenderer>();
                    TR.receiveShadows = false;
                    TR.time = 1;
                    TR.widthMultiplier = 0.1f;
                    TR.startColor = Color.black;
                    TR.endColor = Color.black;
                    TR.alignment = LineAlignment.Local;
                    TR.material = new Material(Shader.Find("Particles/Alpha Blended Premultiply"));
                }
            }

            // Bebug.log to provide response to user
            Debug.Log("Model Configured");
        }
        else
        {
            // Bebug.log to provide response to user
            Debug.Log("No model found with given name. Please enter a valid name.");
        }
    }

    // Function to add the basic UI
    private static void SetupBasicUI()
    {
        // Create an event system obvject
        GameObject ES = new GameObject("EventSystem");
        Undo.RegisterCreatedObjectUndo(ES, "UI Creation");
        ES.transform.position = new Vector3(0, 0, 0);
        // Add the necessary components to it
        ES.AddComponent<EventSystem>();
        ES.AddComponent<StandaloneInputModule>();

        // Create an canvas obvject
        GameObject newCanvas = new GameObject("Canvas");
        Undo.RegisterCreatedObjectUndo(newCanvas, "UI Creation");
        // Add the necessary components to it and setup to it
        Canvas c = newCanvas.AddComponent<Canvas>();
        c.renderMode = RenderMode.ScreenSpaceOverlay;
        newCanvas.AddComponent<CanvasScaler>();
        newCanvas.AddComponent<GraphicRaycaster>();

        // Create a panel UI element to serve as the parent for the UI
        GameObject panel = new GameObject("Panel");
        Undo.RegisterCreatedObjectUndo(panel, "UI Creation");
        // Add the necessary components
        panel.AddComponent<CanvasRenderer>();
        Image i = panel.AddComponent<Image>();
        // Set the properties
        i.color = Color.red;
        RectTransform rt = panel.GetComponent<RectTransform>();
        rt.sizeDelta = new Vector2(200, 200);
        rt.anchorMin = new Vector2(1, 0);
        rt.anchorMax = new Vector2(1, 0);
        rt.pivot = new Vector2(0.5f, 0.5f);
        // Set the parent
        panel.transform.position = new Vector3(-130, 122, 0);
        panel.transform.SetParent(newCanvas.transform, false);

        // Create a text element to hold the speed
        GameObject SpeedText = new GameObject("SpeedText");
        Undo.RegisterCreatedObjectUndo(SpeedText, "UI Creation");
        // Add the necessary components
        SpeedText.AddComponent<CanvasRenderer>();
        Text t = SpeedText.AddComponent<Text>();
        // Set the properties
        t.text = "Speed: " + 0;
        RectTransform rt2 = SpeedText.GetComponent<RectTransform>();
        rt2.sizeDelta = new Vector2(200, 30);
        rt2.anchorMin = new Vector2(0.5f, 1);
        rt2.anchorMax = new Vector2(0.5f, 1);
        rt2.pivot = new Vector2(0.5f, 0.5f);
        // Set the parent
        SpeedText.transform.position = new Vector3(0, -15, 0);
        SpeedText.transform.SetParent(panel.transform, false);

        // Create a text element to hold the gear
        GameObject GearText = new GameObject("GearText");
        Undo.RegisterCreatedObjectUndo(GearText, "UI Creation");
        // Add the necessary components
        GearText.AddComponent<CanvasRenderer>();
        Text t2 = GearText.AddComponent<Text>();
        t2.text = "Gear: " + 0;
        // Set the properties
        RectTransform rt3 = GearText.GetComponent<RectTransform>();
        rt3.sizeDelta = new Vector2(200, 30);
        rt3.anchorMin = new Vector2(0.5f, 1);
        rt3.anchorMax = new Vector2(0.5f, 1);
        rt3.pivot = new Vector2(0.5f, 0.5f);
        // Set the parent
        GearText.transform.position = new Vector3(0, -45, 0);
        GearText.transform.SetParent(panel.transform, false);

        // Create a text element to hold the RPM
        GameObject RPMText = new GameObject("RPMText");
        Undo.RegisterCreatedObjectUndo(RPMText, "UI Creation");
        // Add the necessary components
        RPMText.AddComponent<CanvasRenderer>();
        Text t3 = RPMText.AddComponent<Text>();
        t3.text = "RPM: " + 0;
        // Set the properties
        RectTransform rt4 = RPMText.GetComponent<RectTransform>();
        rt4.sizeDelta = new Vector2(200, 30);
        rt4.anchorMin = new Vector2(0.5f, 1);
        rt4.anchorMax = new Vector2(0.5f, 1);
        rt4.pivot = new Vector2(0.5f, 0.5f);
        // Set the parent
        RPMText.transform.position = new Vector3(0, -75, 0);
        RPMText.transform.SetParent(panel.transform, false);

        // Create a text element to hold the boost
        GameObject BoostText = new GameObject("BoostText");
        Undo.RegisterCreatedObjectUndo(BoostText, "UI Creation");
        // Add the necessary components
        BoostText.AddComponent<CanvasRenderer>();
        Text t4 = BoostText.AddComponent<Text>();
        t4.text = "Boost: " + "False";
        // Set the properties
        RectTransform rt5 = BoostText.GetComponent<RectTransform>();
        rt5.sizeDelta = new Vector2(200, 30);
        rt5.anchorMin = new Vector2(0.5f, 1);
        rt5.anchorMax = new Vector2(0.5f, 1);
        rt5.pivot = new Vector2(0.5f, 0.5f);
        // Set the parent
        BoostText.transform.position = new Vector3(0, -105, 0);
        BoostText.transform.SetParent(panel.transform, false);

        // Bebug.log to provide response to user
        Debug.Log("UI created");
    }

    // Function to setup all the scripts tales a string arguement for the name
    private static void ConfigureScripts(string a_name)
    {
        // Find the object with the parameter name
        GameObject obj = GameObject.Find(a_name);
        // Safety null check
        if (obj != null)
        {
            // Add the Anti roll scripts
            Undo.SetCurrentGroupName("Script Setup");
            AntiRoll[] AR = obj.GetComponents<AntiRoll>();
            // Loop through and set the properties
            for (int i = 0, j = 0; i < AR.Length; i++)
            {
                AR[i].xWheelLeft = GameObject.Find("WheelCollider" + "_" + j).GetComponent<WheelCollider>();
                AR[i].xWheelRight = GameObject.Find("WheelCollider" + "_" + (j + 1)).GetComponent<WheelCollider>();
                j = j + 2;
            }
            // Add the wing script and set the proerty
            Wing W = obj.GetComponent<Wing>();
            W.fLiftCoefficient = 10;
            // Add the suspension script and set the proerties
            Suspension S = obj.GetComponent<Suspension>();
            // Set the wheel colliders
            S.xWheelColliders = new WheelCollider[iAmountOfWheels];
            if(iAmountOfWheels != 0)
            {
                for (int i = 0; i < iAmountOfWheels; i++)
                {
                    S.xWheelColliders[i] = GameObject.Find("WheelCollider" + "_" + i).GetComponent<WheelCollider>();
                }
            }
            // Set the wheels
            S.xWheels = new Transform[iAmountOfWheels];
            if (iAmountOfWheels != 0)
            {
                for (int i = 0; i < iAmountOfWheels; i++)
                {
                    S.xWheels[i] = GameObject.Find("WheelParent" + "_" + i).transform;
                }
            }
            // Add the skid trails script and set the proerties
            SkidTrails[] ST = obj.GetComponents<SkidTrails>();
            if (iAmountOfSkidTrails != 0)
            {
                for (int i = 0; i < iAmountOfSkidTrails; i++)
                {
                    ST[i].xTargetWheel = S.xWheelColliders[i];
                    ST[i].xTrail = GameObject.Find("SkidTrail" + "_" + i);
                }
            }
            // Add the vehicle controller script and set the proerties
            VehicleController V = obj.GetComponent<VehicleController>();
            V.xWheelColliders = new WheelCollider[iAmountOfWheels];
            if (iAmountOfWheels != 0)
            {
                for (int i = 0; i < iAmountOfWheels; i++)
                {
                    V.xWheelColliders[i] = GameObject.Find("WheelCollider" + "_" + i).GetComponent<WheelCollider>();
                }
            }
            // Vehicle setup
            V.sModelName = sModelName;
            V.bIsManual = bIsManual;
            V.eDriveType = (VehicleController.Drivetype)iDriveType;
            V.iBHP = iBHP;
            V.iBrakeTorque = iBrakeTorque;
            V.iMaxSpeed = iMaxSpeed;
            V.iMaxReverseSpeed = iMaxReverseSpeed;
            V.iSteerAngleHighSpeed = iSteerAngleHighSpeed;
            V.iSteerAngleHighSpeedThreshold = iSteerAngleHighSpeedThreshold;
            V.iSteerAngleLowSpeed = iSteerAngleLowSpeed;
            // Gear Ratio setup
            V.fGearRatios = new float[5];
            V.fGearRatios[0] = 2.4f;
            V.fGearRatios[1] = 2f;
            V.fGearRatios[2] = 1.5f;
            V.fGearRatios[3] = 1.2f;
            V.fGearRatios[4] = 1f;
            // HUD Setup - Speed, Gear, RPM & Boost
            V.xHUD = new Text[4];
            V.xHUD[0] = GameObject.Find("SpeedText").GetComponent<Text>();
            V.xHUD[1] = GameObject.Find("GearText").GetComponent<Text>();
            V.xHUD[2] = GameObject.Find("RPMText").GetComponent<Text>();
            V.xHUD[3] = GameObject.Find("BoostText").GetComponent<Text>();
            // Light objects setup
            V.xHeadLightsOBjects = new Renderer[iAmountOfLights_Head];
            if (iAmountOfLights_Head != 0)
            {
                for (int i = 0; i < iAmountOfLights_Head; i++)
                {
                    V.xHeadLightsOBjects[i] = GameObject.Find("SL_Container_" + i).GetComponent<Renderer>();
                }
            }
            V.xBrakeLightsOBjects = new Renderer[iAmountOfLights_Brake];
            if (iAmountOfLights_Brake != 0)
            {
                for (int i = 0; i < iAmountOfLights_Brake; i++)
                {
                    V.xBrakeLightsOBjects[i] = GameObject.Find("BL_Container_" + i).GetComponent<Renderer>();
                }
            }
            V.xReverseLightsOBjects = new Renderer[iAmountOfLights_Reverse];
            if (iAmountOfLights_Reverse != 0)
            {
                for (int i = 0; i < iAmountOfLights_Reverse; i++)
                {
                    V.xReverseLightsOBjects[i] = GameObject.Find("RL_Container_" + i).GetComponent<Renderer>();
                }
            }
            // Headlight - Lights setup
            V.xHeadLights = new GameObject[iAmountOfLights_Head];
            if (iAmountOfLights_Head != 0)
            {
                for (int i = 0; i < iAmountOfLights_Head; i++)
                {
                    V.xHeadLights[i] = GameObject.Find("Spotlight_" + i);
                    V.xHeadLights[i].SetActive(false);
                }
            }
            // Setup and add exhaust functionality
            V.xExhaust = new GameObject[iAmountOfExhaustandBoostParticles];
            if (iAmountOfExhaustandBoostParticles != 0)
            {
                for (int i = 0; i < iAmountOfExhaustandBoostParticles; i++)
                {
                    V.xExhaust[i] = GameObject.Find("Exhaust_PL_" + i);
                }
            }
            // Setup and add boost functionality
            if(bAddBoost == true)
            {
                V.xBoostParticles = new GameObject[iAmountOfExhaustandBoostParticles];
            }
            if(iAmountOfExhaustandBoostParticles != 0 && bAddBoost == true)
            {
                for (int i = 0; i < iAmountOfExhaustandBoostParticles; i++)
                {
                    V.xBoostParticles[i] = GameObject.Find("Boost_PL_" + i);
                    V.xBoostParticles[i].SetActive(false);
                }
            }

            // Bebug.log to provide response to user
            Debug.Log("Setup configured");
        }
        else
        {
            // Bebug.log to provide response to user
            Debug.Log("No hub found with given name. Please enter a valid name.");
        }
    }

    // Void ONGUI used to draw window GUI elements
    private void OnGUI()
    {
        // Update the width
        fWidth = position.width - 5;
        // Set the selected tab
        iselectedAction = GUILayout.SelectionGrid(iselectedAction, sActionLabels, 3, GUILayout.Width(fWidth), GUILayout.Height(fHeight));
        // Load the logo
        xLogo = Resources.Load<Texture2D>("Wheicle_logo_Editor");

        switch (iselectedAction)
        {
            // Tab 0 - Welcome screen combines GUI elements to create functional layout
            case 0:
                {
                    // Wheicle Logo Image
                    Rect xLogoRect = new Rect(34, 40, 370, 120);
                    GUI.DrawTexture(xLogoRect, xLogo);
                    GUILayout.Space(120);
                    // Introduction text
                    EditorGUILayout.LabelField("Welcome", EditorStyles.boldLabel);
                    EditorGUILayout.LabelField("Hello and thank you for downloading Wheicle - The Vehicle Rigger Package!", EditorStyles.wordWrappedLabel);
                    GUILayout.Space(10);
                    EditorGUILayout.LabelField("Above you can see a selection of headings that will help you to use the tool to setup your vehicle.", EditorStyles.wordWrappedLabel);
                    GUILayout.Space(10);
                    EditorGUILayout.LabelField("-> Welcome - This Page! It contains general information.", EditorStyles.wordWrappedLabel);
                    EditorGUILayout.LabelField("-> Rig Vehicle - Where the magic happens!", EditorStyles.wordWrappedLabel);
                    EditorGUILayout.LabelField("-> Information - The page for detailed information.", EditorStyles.wordWrappedLabel);
                    GUILayout.Space(10);
                    // Link to YouTube walkthrough of asset
                    EditorGUILayout.LabelField("A tutorial can be found at this link. New users are advised to follow along.", EditorStyles.wordWrappedLabel);
                    if (GUILayout.Button("Open Tutorial"))
                    {
                        Application.OpenURL("https://youtu.be/lK-n071EjHA");
                    }
                    GUILayout.Space(10);
                    EditorGUILayout.LabelField("For any further queries contact me at Contact@AbbasKhan.co.uk", EditorStyles.wordWrappedLabel);
                    break;
                }
            // Tab 1 - Rigger screen combines GUI elements to create functional layout
            case 1:
                {
                    // Scroll bar
                    xUIScroll = EditorGUILayout.BeginScrollView(xUIScroll);
                    EditorGUILayout.LabelField("Rig Vehicle", EditorStyles.boldLabel);
                    EditorGUILayout.LabelField("Please use the buttons and input fields below to setup your vehicle!", EditorStyles.wordWrappedLabel);
                    
                    // Hub creation
                    #region 1. Hub Creation
                    GUILayout.Space(3);
                    EditorGUILayout.LabelField("1. Hub Creation", EditorStyles.boldLabel);
                    // Foldout UI
                    bCreateNewHub = EditorGUILayout.Foldout(bCreateNewHub, "Create New Hub");
                    if (bCreateNewHub == true)
                    {
                        // Textfield for the new hub name
                        sHubName = EditorGUILayout.TextField("Hub Name:", sHubName);
                        // Button to execute create hub function
                        if (GUILayout.Button("Make HUB"))
                        {
                            CreateHub(sHubName);
                        }
                    }
                    EditorGUILayout.LabelField("Upon creation of the HUB, add your vehicle model as a child object with position 0,0,0", EditorStyles.wordWrappedLabel);
                    EditorGUILayout.Separator();
                    #endregion

                    // Camera Setup
                    #region 2. Camera Setup
                    GUILayout.Space(3);
                    EditorGUILayout.LabelField("2. Camera Setup", EditorStyles.boldLabel);
                    // Foldout UI
                    bConfigureCamera = EditorGUILayout.Foldout(bConfigureCamera, "Configure Camera");
                    if (bConfigureCamera == true)
                    {
                        EditorGUILayout.LabelField("Add the camera you wish to use for the vehicle", EditorStyles.wordWrappedLabel);
                        // Object field used to place the camera that the user wishes to setup
                        xCamera = EditorGUILayout.ObjectField("Camera:", xCamera, typeof(GameObject), true) as GameObject;
                        // Text field for the hub name
                        sHubName = EditorGUILayout.TextField("Hub Name:", sHubName);
                        // Button used to setup the camera
                        if (GUILayout.Button("Configure Camera"))
                        {
                            SetupCamera(xCamera.name);
                        }
                    }
                    EditorGUILayout.Separator();
                    #endregion

                    // Hub Setup
                    #region 3. Hub Setup
                    GUILayout.Space(3);
                    EditorGUILayout.LabelField("3. Hub Setup", EditorStyles.boldLabel);
                    // Foldout UI
                    bConfigureHub = EditorGUILayout.Foldout(bConfigureHub, "Configure Hub");
                    if (bConfigureHub == true)
                    {
                        // Text field used to store the hub name
                        sHubName = EditorGUILayout.TextField("Hub Name:", sHubName);
                        // Button to execute the add rigidbody function
                        if (GUILayout.Button("Add Main Hub RigidBody"))
                        {
                            SetupMainHubRigidBody(sHubName);
                        }
                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField("Amount of wheels: (Even & Min 4)", EditorStyles.wordWrappedLabel);
                        // Int field used to hold the amount of wheels (Mathf.Abs to prevent negative values)
                        iAmountOfWheels = Mathf.Abs(EditorGUILayout.IntField(iAmountOfWheels));
                        // Safe gaurd to make sure it is even and more than 4
                        if (iAmountOfWheels % 2 != 0 || iAmountOfWheels < 4)
                        {
                            iAmountOfWheels = 0;
                        }
                        EditorGUILayout.EndHorizontal();
                        // Button used execute the add wheel colliders function
                        if (GUILayout.Button("Add Main Hub Wheel Colliders"))
                        {
                            AddWheelColliders(sHubName);
                        }
                        // Foldout UI
                        bAddSkidTrails = EditorGUILayout.Toggle("Add Skid Trails Script", bAddSkidTrails);
                        if (bAddSkidTrails == true)
                        {
                            EditorGUILayout.BeginHorizontal();
                            EditorGUILayout.LabelField("Amount of trails:", EditorStyles.wordWrappedLabel);
                            // Int field used to hold the amount of skid trails (Mathf.Abs to prevent negative values)
                            iAmountOfSkidTrails = Mathf.Abs(EditorGUILayout.IntField(iAmountOfSkidTrails));
                            EditorGUILayout.EndHorizontal();
                        }
                        // Button used to execute the main hub setup function
                        if (GUILayout.Button("Configure Hub"))
                        {
                            SetupMainHub(sHubName);
                        }
                    }
                    EditorGUILayout.Separator();
                    #endregion

                    // Model Setup
                    #region 4. Model Setup
                    GUILayout.Space(3);
                    EditorGUILayout.LabelField("4. Model Setup", EditorStyles.boldLabel);
                    // Foldout UI
                    bConfigureModel = EditorGUILayout.Foldout(bConfigureModel, "Configure Model");
                    if (bConfigureModel == true)
                    {
                        // Text field used to hold the model name
                        sModelName = EditorGUILayout.TextField("Model Name:", sModelName);
                        // Button used to execute the setup main collider function
                        if (GUILayout.Button("Add Model Collider"))
                        {
                            SetupMainCollider(sModelName);
                        }

                        // Toggle UI
                        bAddWheels = EditorGUILayout.Toggle("Add Wheels", bAddWheels);
                        if (bAddWheels == true)
                        {
                            EditorGUILayout.LabelField("Add the prefabs you'd like to use for the left and right side.", EditorStyles.wordWrappedLabel);
                            // Object field used to store the wheels for either side
                            xWheelPrefab_L = EditorGUILayout.ObjectField("Wheel for the Left-Side", xWheelPrefab_L, typeof(GameObject), false) as GameObject;
                            xWheelPrefab_R = EditorGUILayout.ObjectField("Wheel for the Right-Side", xWheelPrefab_R, typeof(GameObject), false) as GameObject;
                        }
                        EditorGUILayout.Separator();
                        // Toggle UI
                        bAddExhaust = EditorGUILayout.Toggle("Add Exhaust System", bAddExhaust);
                        if(bAddExhaust == true)
                        {
                            EditorGUILayout.BeginHorizontal();
                            EditorGUILayout.LabelField("Amount of exhausts:", EditorStyles.wordWrappedLabel);
                            // Int field used to hold the amount of exhausts (Mathf.Abs to prevent negative values)
                            iAmountOfExhaustandBoostParticles = Mathf.Abs(EditorGUILayout.IntField(iAmountOfExhaustandBoostParticles));
                            EditorGUILayout.EndHorizontal();
                            // Toggle UI
                            bAddBoost = EditorGUILayout.Toggle("Add Boost Functionality", bAddBoost);
                            // Button used to change the size pf the array
                            if (GUILayout.Button("Attach to existing objects?"))
                            {
                                xExhaust_Attatchments = new GameObject[iAmountOfExhaustandBoostParticles];
                            }
                            if(iAmountOfExhaustandBoostParticles == xExhaust_Attatchments.Length)
                            {
                                EditorGUILayout.LabelField("If you do not have an attatchment model, one will be created for you.", EditorStyles.wordWrappedLabel);
                                for (int i = 0; i < iAmountOfExhaustandBoostParticles; i++)
                                {
                                    // Object field used to store the attatchment points
                                    xExhaust_Attatchments[i] = EditorGUILayout.ObjectField("Exhaust", xExhaust_Attatchments[i], typeof(GameObject), true) as GameObject;
                                    if(xExhaust_Attatchments[i] != null)
                                    {
                                        // Persistence safety check to prevent assets not in the scene being assigned
                                        bool persistent = EditorUtility.IsPersistent(xExhaust_Attatchments[i]);
                                        if (persistent == true)
                                        {
                                            xExhaust_Attatchments[i] = null;
                                            // Bebug.log to provide response to user
                                            Debug.Log("Add attatchment object from the vehicle in the scene.");
                                        }
                                    }
                                }
                            }
                        }
                        EditorGUILayout.Separator();
                        // Toggle UI
                        bAddHeadlights = EditorGUILayout.Toggle("Add Headlights", bAddHeadlights);
                        if (bAddHeadlights == true)
                        {
                            EditorGUILayout.BeginHorizontal();
                            EditorGUILayout.LabelField("Amount of head lights:", EditorStyles.wordWrappedLabel);
                            // Int field used to hold the amount of head lights (Mathf.Abs to prevent negative values)
                            iAmountOfLights_Head = Mathf.Abs(EditorGUILayout.IntField(iAmountOfLights_Head));
                            EditorGUILayout.EndHorizontal();
                            // Button used to change the size pf the array
                            if (GUILayout.Button("Attach to existing objects?"))
                            {
                                xHeadlights_Attatchments = new GameObject[iAmountOfLights_Head];
                            }
                            if (iAmountOfLights_Head == xHeadlights_Attatchments.Length)
                            {
                                EditorGUILayout.LabelField("If you do not have an attatchment model, one will be created for you.", EditorStyles.wordWrappedLabel);
                                for (int i = 0; i < iAmountOfLights_Head; i++)
                                {
                                    // Object field used to store the attatchment points
                                    xHeadlights_Attatchments[i] = EditorGUILayout.ObjectField("Head Lights", xHeadlights_Attatchments[i], typeof(GameObject), true) as GameObject;
                                    if (xHeadlights_Attatchments[i] != null)
                                    {
                                        // Persistence safety check to prevent assets not in the scene being assigned
                                        bool persistent = EditorUtility.IsPersistent(xHeadlights_Attatchments[i]);
                                        if (persistent == true)
                                        {
                                            xHeadlights_Attatchments[i] = null;
                                            // Bebug.log to provide response to user
                                            Debug.Log("Add attatchment object from the vehicle in the scene.");
                                        }
                                    }
                                }
                            }
                        }
                        EditorGUILayout.Separator();
                        // Toggle UI
                        bAddReverselights = EditorGUILayout.Toggle("Add Reverse lights", bAddReverselights);
                        if (bAddReverselights == true)
                        {
                            EditorGUILayout.BeginHorizontal();
                            EditorGUILayout.LabelField("Amount of reverse lights:", EditorStyles.wordWrappedLabel);
                            // Int field used to hold the amount of reverse lights (Mathf.Abs to prevent negative values)
                            iAmountOfLights_Reverse = Mathf.Abs(EditorGUILayout.IntField(iAmountOfLights_Reverse));
                            EditorGUILayout.EndHorizontal();
                            // Button used to change the size pf the array
                            if (GUILayout.Button("Attach to existing objects?"))
                            {
                                xReverselights_Attatchments = new GameObject[iAmountOfLights_Reverse];
                            }
                            if (iAmountOfLights_Reverse == xReverselights_Attatchments.Length)
                            {
                                EditorGUILayout.LabelField("If you do not have an attatchment model, one will be created for you.", EditorStyles.wordWrappedLabel);
                                for (int i = 0; i < iAmountOfLights_Reverse; i++)
                                {
                                    // Object field used to store the attatchment points
                                    xReverselights_Attatchments[i] = EditorGUILayout.ObjectField("Reverse Lights", xReverselights_Attatchments[i], typeof(GameObject), true) as GameObject;
                                    if (xReverselights_Attatchments[i] != null)
                                    {
                                        // Persistence safety check to prevent assets not in the scene being assigned
                                        bool persistent = EditorUtility.IsPersistent(xReverselights_Attatchments[i]);
                                        if (persistent == true)
                                        {
                                            xReverselights_Attatchments[i] = null;
                                            // Bebug.log to provide response to user
                                            Debug.Log("Add attatchment object from the vehicle in the scene.");
                                        }
                                    }
                                }
                            }
                        }
                        EditorGUILayout.Separator();
                        // Toggle UI
                        bAddBrakelights = EditorGUILayout.Toggle("Add Brake lights", bAddBrakelights);
                        if (bAddBrakelights == true)
                        {
                            EditorGUILayout.BeginHorizontal();
                            EditorGUILayout.LabelField("Amount of brake lights:", EditorStyles.wordWrappedLabel);
                            // Int field used to hold the amount of brake lights (Mathf.Abs to prevent negative values)
                            iAmountOfLights_Brake = Mathf.Abs(EditorGUILayout.IntField(iAmountOfLights_Brake));
                            EditorGUILayout.EndHorizontal();
                            // Button used to change the size pf the array
                            if (GUILayout.Button("Attach to existing objects?"))
                            {
                                xBrakelights_Attatchments = new GameObject[iAmountOfLights_Brake];
                            }
                            if (iAmountOfLights_Brake == xBrakelights_Attatchments.Length)
                            {
                                EditorGUILayout.LabelField("If you do not have an attatchment model, one will be created for you.", EditorStyles.wordWrappedLabel);
                                for (int i = 0; i < iAmountOfLights_Brake; i++)
                                {
                                    // Object field used to store the attatchment points
                                    xBrakelights_Attatchments[i] = EditorGUILayout.ObjectField("Brake Lights", xBrakelights_Attatchments[i], typeof(GameObject), true) as GameObject;
                                    if (xBrakelights_Attatchments[i] != null)
                                    {
                                        // Persistence safety check to prevent assets not in the scene being assigned
                                        bool persistent = EditorUtility.IsPersistent(xBrakelights_Attatchments[i]);
                                        if (persistent == true)
                                        {
                                            xBrakelights_Attatchments[i] = null;
                                            // Bebug.log to provide response to user
                                            Debug.Log("Add attatchment object from the vehicle in the scene.");
                                        }
                                    }
                                }
                            }
                        }
                        EditorGUILayout.Separator();
                        // Toggle UI
                        bAddSkidTrails = EditorGUILayout.Toggle("Add Skid Trails", bAddSkidTrails);
                        // Button used to execute the configure model function and add the components that the user has specified
                        if (GUILayout.Button("Configure Model"))
                        {
                            AddComponentsModel(sModelName);
                        }
                    }
                    EditorGUILayout.Separator();
                    #endregion

                    // Vehicle Setup
                    #region 5. Vehicle Setup
                    GUILayout.Space(3);
                    EditorGUILayout.LabelField("5. Vehicle Setup", EditorStyles.boldLabel);
                    // Foldout UI
                    bConfigureSetup = EditorGUILayout.Foldout(bConfigureSetup, "Configure Setup");
                    if(bConfigureSetup == true)
                    {
                        // Button used to add basic UI
                        if (GUILayout.Button("Add basic UI"))
                        {
                            SetupBasicUI();
                        }
                        // Foldout UI
                        bConfigureVehicle = EditorGUILayout.Foldout(bConfigureVehicle, "Configure Vehicle");
                        if (bConfigureVehicle == true)
                        {
                            // Text field to hold the hub name
                            sHubName = EditorGUILayout.TextField("Hub Name:", sHubName);
                            // Toggle UI
                            bIsManual = EditorGUILayout.Toggle("Manual Transmission?", bIsManual);
                            EditorGUILayout.BeginHorizontal();
                            // Int slider used for the three different drive types
                            EditorGUILayout.LabelField("Drivetype (0 = FWD, 1 = RWD & 2 = AWD)", EditorStyles.wordWrappedLabel);
                            iDriveType = EditorGUILayout.IntSlider(iDriveType, 0, 2);
                            EditorGUILayout.EndHorizontal();
                            // Mathf.Abs acts as safegaurd for values
                            EditorGUILayout.BeginHorizontal();
                            EditorGUILayout.LabelField("BHP:", EditorStyles.label);
                            iBHP = Mathf.Abs(EditorGUILayout.IntField(iBHP));
                            EditorGUILayout.EndHorizontal();
                            EditorGUILayout.BeginHorizontal();
                            EditorGUILayout.LabelField("Brake Torque:", EditorStyles.label);
                            iBrakeTorque = Mathf.Abs(EditorGUILayout.IntField(iBrakeTorque));
                            EditorGUILayout.EndHorizontal();
                            EditorGUILayout.BeginHorizontal();
                            EditorGUILayout.LabelField("Max Speed:", EditorStyles.label);
                            iMaxSpeed = Mathf.Abs(EditorGUILayout.IntField(iMaxSpeed));
                            EditorGUILayout.EndHorizontal();
                            EditorGUILayout.BeginHorizontal();
                            EditorGUILayout.LabelField("Max Reverse Speed:", EditorStyles.label);
                            iMaxReverseSpeed = Mathf.Abs(EditorGUILayout.IntField(iMaxReverseSpeed)) * -1;
                            EditorGUILayout.EndHorizontal();
                            EditorGUILayout.BeginHorizontal();
                            EditorGUILayout.LabelField("Steering Angle at Low Speed:", EditorStyles.label);
                            iSteerAngleLowSpeed = Mathf.Abs(EditorGUILayout.IntField(iSteerAngleLowSpeed));
                            EditorGUILayout.EndHorizontal();
                            EditorGUILayout.BeginHorizontal();
                            EditorGUILayout.LabelField("Steering Angle at High Speed:", EditorStyles.label);
                            iSteerAngleHighSpeed = Mathf.Abs(EditorGUILayout.IntField(iSteerAngleHighSpeed));
                            EditorGUILayout.EndHorizontal();
                            EditorGUILayout.BeginHorizontal();
                            EditorGUILayout.LabelField("Steering Angle Speed Threshold:", EditorStyles.label);
                            iSteerAngleHighSpeedThreshold = Mathf.Abs(EditorGUILayout.IntField(iSteerAngleHighSpeedThreshold));
                            EditorGUILayout.EndHorizontal();
                        }
                        // Button used to configure the various scripts
                        if (GUILayout.Button("Configure Scripts"))
                        {
                            ConfigureScripts(sHubName);
                        }
                        GUILayout.Space(3);
                    }
                    EditorGUILayout.Separator();
                    #endregion

                    EditorGUILayout.LabelField("6. Final Steps", EditorStyles.boldLabel);
                    EditorGUILayout.LabelField("After configuring the scripts, setup is now complete. The following elements will need to be manually adjusted.", EditorStyles.wordWrappedLabel);
                    GUILayout.Space(10);
                    EditorGUILayout.LabelField("-> The BOX Collider on the model to ensure it encompases the vehicle", EditorStyles.wordWrappedLabel);
                    EditorGUILayout.LabelField("-> The wheel collider radius to fit the desired wheel size", EditorStyles.wordWrappedLabel);
                    EditorGUILayout.LabelField("-> the components will need manual positioing as they are currently at 0,0,0", EditorStyles.wordWrappedLabel);
                    GUILayout.Space(10);
                    EditorGUILayout.LabelField("With this done, your vehicle is now complete and ready for use.", EditorStyles.wordWrappedLabel);
                    EditorGUILayout.EndScrollView();
                    break;
                }
            // Tab 2 - Credits/Information screen combines GUI elements to create functional layout
            case 2:
                {
                    // Scroll bar
                    xUIScroll2 = EditorGUILayout.BeginScrollView(xUIScroll2);
                    EditorGUILayout.LabelField("Information", EditorStyles.boldLabel);
                    EditorGUILayout.LabelField("The following sections of information have been outlined for your convenience.", EditorStyles.wordWrappedLabel);
                    GUILayout.Space(3);
                    // Vehicle modelling info
                    EditorGUILayout.LabelField("Vehicle Modelling Information", EditorStyles.boldLabel);
                    EditorGUILayout.LabelField("The following information applies to all modelling software from Autodesk 3DS Max to Blender.", EditorStyles.wordWrappedLabel);
                    EditorGUILayout.LabelField("When you are creating your vehicle model, adhering to the following points will allow for a smooth integration with this plugin.", EditorStyles.wordWrappedLabel);
                    GUILayout.Space(3);
                    EditorGUILayout.LabelField("-> Ensure that each element is seperate to the main body like the example", EditorStyles.wordWrappedLabel);
                    EditorGUILayout.LabelField("-> Keep scale of objects unfiormly 1 (1,1,1)", EditorStyles.wordWrappedLabel);
                    EditorGUILayout.LabelField("-> Ensure wheels are saved as seperate model", EditorStyles.wordWrappedLabel);
                    GUILayout.Space(3);
                    // Attribution info
                    EditorGUILayout.LabelField("Attribution Information", EditorStyles.boldLabel);
                    EditorGUILayout.LabelField("This plugin is licensed under the Creative Commons 3.0 License meaning attribution is required. The textbox below contains all the information required to do this.", EditorStyles.wordWrappedLabel);
                    EditorGUILayout.SelectableLabel("Wheicle - Abbas Khan Games (www.abbaskhan.co.uk)" + "\n" + "Licensed under Creative Commons: By Attribution 3.0 License http://creativecommons.org/licenses/by/3.0/", EditorStyles.wordWrappedMiniLabel);
                    // Author info
                    EditorGUILayout.LabelField("Author Information", EditorStyles.boldLabel);
                    EditorGUILayout.LabelField("I (Abbas Khan) am the creator of this plugin. I've created a variety of different projects over the years and should you wish to find out more about them visit my portfolio at: ", EditorStyles.wordWrappedLabel);
                    EditorGUILayout.SelectableLabel("https://www.abbaskhan.co.uk/", EditorStyles.wordWrappedLabel);
                    GUILayout.Space(3);
                    EditorGUILayout.EndScrollView();
                    break;
                }
        }
    }
}