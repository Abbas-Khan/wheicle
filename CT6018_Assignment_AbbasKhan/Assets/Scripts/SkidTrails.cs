﻿///////////////////////////////////////////////////////////////////
// Name: SkidTrails.cs
// Author: Abbas Khan
// Date: 23/09/18
// Bio: Used to control the SkidTrails of the vehicle
///////////////////////////////////////////////////////////////////

// Using Calls
using System.Collections;
using UnityEngine;

public class SkidTrails : MonoBehaviour
{
    // Whhelcollider used to hold the target wheel for this skid trail
    public WheelCollider xTargetWheel;
    // GameObject reference to the skid trail itself
    public GameObject xTrail;
    // RaycastHit used to get the hit position of the wheel and the ground
    private RaycastHit xRayHit;
    // Vector3 used to hold the center of the collider
    private Vector3 xColliderCenter;
    // Wheel Hit used to get the slip value
    private WheelHit xWheelHit;
    // Floats used to specify the thresholds for activation and deactivation of the skid trail
    private float fSlipUpperThreshold = 0.1f;
    private float fSlipLowerThreshold = 0.075f;
    // Float to hold the offset for the skid trail from the ground
    [SerializeField] private float GroundOffset = 0.01f;

    void Start()
    {
        // Safety null check
        if(xTrail != null)
        {
            // Deactivate the object
            xTrail.SetActive(false);
        }
    }

    void Update()
    {
        // Calculate the collider's center
        xColliderCenter = xTargetWheel.transform.TransformPoint(xTargetWheel.center);

        // Perform a raycast to get contact point
        if (Physics.Raycast(xColliderCenter, -xTargetWheel.transform.up, out xRayHit, xTargetWheel.suspensionDistance + xTargetWheel.radius))
        {
            // Assign the trail's position to that point
            xTrail.transform.position = new Vector3(xRayHit.point.x, xRayHit.collider.bounds.max.y + GroundOffset, xRayHit.point.z);
        }
        else
        {
            // Otherwise, calculate an adjusted value
            xTrail.transform.position = xColliderCenter - (xTargetWheel.transform.up * xTargetWheel.suspensionDistance);
        }

        // Get the ground hit data
        xTargetWheel.GetGroundHit(out xWheelHit);

        if (Mathf.Abs(xWheelHit.sidewaysSlip) > fSlipUpperThreshold)
        {
            // Safety null check
            if (xTrail != null)
            {
                // Activate the object
                xTrail.SetActive(true);
            }
        }
        else if (Mathf.Abs(xWheelHit.sidewaysSlip) <= fSlipLowerThreshold)
        {
            // Start the delayed trail deactivation
            StartCoroutine(CloseSkidTrails());
        }
    }

    // Ienumerator used to delay the turn off of the skid trails
    IEnumerator CloseSkidTrails()
    {
        // Wait for 0.4 seconds
        yield return new WaitForSeconds(0.4f);
        // Safety null check
        if (xTrail != null)
        {
            xTrail.SetActive(false);
        }
    }
}
