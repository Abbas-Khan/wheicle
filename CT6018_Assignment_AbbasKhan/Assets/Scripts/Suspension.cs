﻿///////////////////////////////////////////////////////////////////
// Name: Suspension.cs
// Author: Abbas Khan
// Date: 22/09/18
// Bio: Used to control the suspension of the vehicle
///////////////////////////////////////////////////////////////////

// Using Calls
using UnityEngine;

public class Suspension : MonoBehaviour
{
    // Transform array used to hold the positions of the wheel models
    public Transform[] xWheels;
    // WheelCollider array used to hold the colliders
    public WheelCollider[] xWheelColliders;
    // Vector3 to hold the wheelcollider position
    private Vector3 xPos;
    // Quaternion to hold the wheelcollider rotation
    private Quaternion xRot;

    void Update()
    {
        // Loop until the end of the wheel colliders array (Both amount of wheel colliders and wheels should be equal)
        for (int i = 0; i < xWheelColliders.Length; i++)
        {
            // Safety null check
            if (xWheelColliders[i] != null)
            {
                // Get the rotation and position of the wheel collider
                xWheelColliders[i].GetWorldPose(out xPos, out xRot);
            }
            // Safety null check
            if(xWheels[i] != null)
            {
                // Apply the rotation and position to the models
                xWheels[i].position = xPos;
                xWheels[i].rotation = xRot;
            }
        }
    }
}
