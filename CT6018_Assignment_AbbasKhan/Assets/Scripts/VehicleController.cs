﻿///////////////////////////////////////////////////////////////////
// Name: VehicleController.cs
// Author: Abbas Khan
// Date: 22/09/18
// Bio: Used to control the vehicle
///////////////////////////////////////////////////////////////////

// Using calls
using UnityEngine;
using UnityEngine.UI;

public class VehicleController : MonoBehaviour {

    // Enum to hold the different drive types
    public enum Drivetype
    {
        FrontWheelDrive,
        RearWheelDrive,
        AllWheelDrive
    }
    public Drivetype eDriveType;
    // String to hold the name of the model object
    public string sModelName = "Model";
    // Engine Related Variables
    public int iBHP = 500;
    public int iBrakeTorque = 250;
    public float fTorque;
    public float fEngineRPM;
    public float fGearUpRPM = 500;
    public float fGearDownRPM = 150;
    // Float array for the gear ratios
    public float[] fGearRatios;
    public int iCurrentGear;
    int AppropriateGear;
    // Array to hold the wheel colliders, 0 is Front row Left, 1 is Front row Right, 2 is Second row Left and so on.
    public WheelCollider[] xWheelColliders;
    // Speed Variables
    public float fCurrentSpeed;
    public int iMaxSpeed = 350;
    public int iMaxReverseSpeed = -40;
    // Steering Variables
    private float fSteerAngle;
    public int iSteerAngleLowSpeed = 13;
    public int iSteerAngleHighSpeed = 2;
    public int iSteerAngleHighSpeedThreshold = 100;
    // Renderer arrays to hold the light materials
    public Renderer[] xReverseLightsOBjects;
    public Renderer[] xBrakeLightsOBjects;
    public Renderer[] xHeadLightsOBjects;
    // Gameobject array to hold the actual lights
    public GameObject[] xHeadLights;
    // Gameobject arrays to hold the particle systems
    public GameObject[] xExhaust;
    public GameObject[] xBoostParticles;
    // Gameobject arrays to hold the UI text elements
    public Text[] xHUD;
    // Gameobject to hold the model object
    private GameObject xModelObject;
    // Booleans to hold the state of the vehicle
    public bool bHandBraked = false;
    public bool bIsManual = false;
    public bool bBoostOn = false;
    public bool bLightsOn = false;

    void Start()
    {
        // Find the model object
        xModelObject = GameObject.Find(sModelName);
        // Calculate the center of mass
        GetComponent<Rigidbody>().centerOfMass = new Vector3(xModelObject.transform.localPosition.x * transform.localScale.x, xModelObject.transform.localPosition.y * transform.localScale.y, xModelObject.transform.localPosition.z * transform.localScale.z);
    }

    void Update()
    {
        // Update the UI, if they are not null
        if(xHUD.Length > 0)
        {
            if (xHUD[0].gameObject != null)
            {
                xHUD[0].text = ("Speed: " + fCurrentSpeed);
            }
            if (xHUD[1].gameObject != null)
            {
                xHUD[1].text = ("Gear: " + (iCurrentGear + 1));
            }
            if (xHUD[2].gameObject != null)
            {
                xHUD[2].text = ("RPM: " + Mathf.Abs(fEngineRPM));
            }
            if (xHUD[3].gameObject != null)
            {
                xHUD[3].text = ("Boost: " + bBoostOn);
            }
        }

        // Apply the steering & Acceleration
        ApplySteering();
        ApplyAcceleration();

        // If the boost is off
        if (bBoostOn == false)
        {
            // Calculate the current speed with a smaller multiplier
            fCurrentSpeed = GetComponent<Rigidbody>().velocity.magnitude * 3.6f;
        }
        else
        {
            // Otherwise, calculate the current speed with a larger multiplier
            fCurrentSpeed = GetComponent<Rigidbody>().velocity.magnitude * 4.2f;
        }

        // Calculate the engine RPM and Torque
        fEngineRPM = (Mathf.Round((xWheelColliders[Random.Range(0,xWheelColliders.Length)].rpm * fGearRatios[iCurrentGear])));
        fTorque = iBHP * fGearRatios[iCurrentGear];

        // If it's a manual
        if (bIsManual == true)
        {
            // Perform manual transmission
            ManualGearTransmission();
        }
        else
        {
            // Otherwise, perform automatic transmission
            AutomaticGearTransmission();
        }

        // If space bar is pressed
        if (Input.GetButton("Jump"))
        {
            // Update the brake light object's renderer colour
            for (int i = 0; i < xBrakeLightsOBjects.Length; i++)
            {
                if (xBrakeLightsOBjects[i].gameObject != null)
                {
                    xBrakeLightsOBjects[i].material.color = Color.red;
                }
            }
            // Apply the hand brakes
            ApplyHandBrakes();
        }
        else
        {
            // Otherwise, reset the renderer
            for (int i = 0; i < xBrakeLightsOBjects.Length; i++)
            {
                if (xBrakeLightsOBjects[i].gameObject != null)
                {
                    xBrakeLightsOBjects[i].material.color = Color.grey;
                }
            }
        }

        // If the R key is pressed
        if (Input.GetKeyDown(KeyCode.R))
        {
            // Reset the position and rotation
            transform.position = new Vector3(transform.position.x, transform.position.y + 2, transform.position.z);
            transform.rotation = Quaternion.identity;
        }

        // If the current speed is greater than 20
        if(fCurrentSpeed > 20)
        {
            // Turn off the exhaust particles
            for (int i = 0; i < xExhaust.Length; i++)
            {
                if (xExhaust[i].gameObject != null)
                {
                    xExhaust[i].SetActive(false);
                }
            }
        }
        else
        {
            // Otherwise, turn them on
            for (int i = 0; i < xExhaust.Length; i++)
            {
                if (xExhaust[i].gameObject != null)
                {
                    xExhaust[i].SetActive(true);
                }
            }
        }

        // If the S key is pressed
        if (Input.GetKey(KeyCode.S))
        {
            fCurrentSpeed = fCurrentSpeed * -1;
            // Update the reverse light object's renderer colour
            for (int i = 0; i < xReverseLightsOBjects.Length; i++)
            {
                if (xReverseLightsOBjects[i].gameObject != null)
                {
                    xReverseLightsOBjects[i].material.color = Color.white;
                }
            }
        }
        else
        {
            // Otherwise, reset the renderer
            for (int i = 0; i < xReverseLightsOBjects.Length; i++)
            {
                if (xReverseLightsOBjects[i].gameObject != null)
                {
                    xReverseLightsOBjects[i].material.color = Color.grey;
                }
            }
        }

        // If the L key is pressed
        if (Input.GetKeyDown(KeyCode.L))
        {
            if(bLightsOn == false)
            {
                // Update the head light object's renderer colour
                for (int i = 0; i < xHeadLightsOBjects.Length; i++)
                {
                    if (xHeadLightsOBjects[i].gameObject != null)
                    {
                        xHeadLightsOBjects[i].material.color = Color.white;
                    }
                }
                // Turn on the lights
                for (int i = 0; i < xHeadLights.Length; i++)
                {
                    if (xHeadLights[i].gameObject != null)
                    {
                        xHeadLights[i].SetActive(true);
                        bLightsOn = true;
                    }
                }
            }
            else if(bLightsOn == true)
            {
                // Otherwise, reset the renderer
                for (int i = 0; i < xHeadLightsOBjects.Length; i++)
                {
                    if (xHeadLightsOBjects[i].gameObject != null)
                    {
                        xHeadLightsOBjects[i].material.color = Color.grey;
                    }
                }
                // Turn off the lights
                for (int i = 0; i < xHeadLights.Length; i++)
                {
                    if (xHeadLights[i].gameObject != null)
                    {
                        xHeadLights[i].SetActive(false);
                        bLightsOn = false;
                    }
                }
            }
        }

        // If the B key is pressed
        if (Input.GetKeyDown(KeyCode.B))
        {
            if (bBoostOn == false)
            {
                // Turn on the boost particles
                for (int i = 0; i < xBoostParticles.Length; i++)
                {
                    if (xBoostParticles[i].gameObject != null)
                    {
                        xBoostParticles[i].SetActive(true);
                        bBoostOn = true;
                    }
                }
            }
            else if (bBoostOn == true)
            {
                // Turn off the boost particles
                for (int i = 0; i < xBoostParticles.Length; i++)
                {
                    if (xBoostParticles[i].gameObject != null)
                    {
                        xBoostParticles[i].SetActive(false);
                        bBoostOn = false;
                    }
                }
            }
        }

        // Boost particle safety check to prevent boost particle reaming on in low speed
        if(fCurrentSpeed < 50)
        {
            for (int i = 0; i < xBoostParticles.Length; i++)
            {
                if (xBoostParticles[i].gameObject != null)
                {
                    xBoostParticles[i].SetActive(false);
                    bBoostOn = false;
                }
            }
        }
    }

    // Function to apply an acceleration force
    void ApplyAcceleration()
    {
        // If the cyrrent speed & engine RPM are within bounds
        if (fCurrentSpeed < iMaxSpeed && fCurrentSpeed > iMaxReverseSpeed && fEngineRPM <= fGearUpRPM)
        {
            if(eDriveType == Drivetype.AllWheelDrive)
            {
                // Loop through the wheels, reset the braking force
                for (int i = 0; i < xWheelColliders.Length; i++)
                {
                    // Apply the acceleration force
                    xWheelColliders[i].motorTorque = fTorque * Input.GetAxis("Vertical");
                    xWheelColliders[i].brakeTorque = 0;
                }
            }
            else if (eDriveType == Drivetype.FrontWheelDrive)
            {
                // Apply the acceleration force
                xWheelColliders[0].motorTorque = fTorque * Input.GetAxis("Vertical");
                xWheelColliders[1].motorTorque = fTorque * Input.GetAxis("Vertical");

                // Loop through the wheels, reset the braking force
                for (int i = 0; i < xWheelColliders.Length; i++)
                {
                    xWheelColliders[i].brakeTorque = 0;
                }
            }
            else if (eDriveType == Drivetype.RearWheelDrive)
            {
                // Apply the acceleration force
                xWheelColliders[xWheelColliders.Length - 2].motorTorque = fTorque * Input.GetAxis("Vertical");
                xWheelColliders[xWheelColliders.Length - 1].motorTorque = fTorque * Input.GetAxis("Vertical");

                // Loop through the wheels, reset the braking force
                for (int i = 0; i < xWheelColliders.Length; i++)
                {
                    xWheelColliders[i].brakeTorque = 0;
                }
            }
        }
        else
        {
            if (eDriveType == Drivetype.AllWheelDrive)
            {
                // Apply the braking force and reset the acceleration force
                for (int i = 0; i < xWheelColliders.Length; i++)
                {
                    xWheelColliders[i].motorTorque = 0;
                    xWheelColliders[i].brakeTorque = iBrakeTorque;
                }
            }
            else if (eDriveType == Drivetype.FrontWheelDrive)
            {
                // Apply the braking force and reset the acceleration force
                xWheelColliders[0].motorTorque = 0;
                xWheelColliders[0].brakeTorque = iBrakeTorque;
                xWheelColliders[1].motorTorque = 0;
                xWheelColliders[1].brakeTorque = iBrakeTorque;
            }
            else if (eDriveType == Drivetype.RearWheelDrive)
            {
                // Apply the braking force and reset the acceleration force
                xWheelColliders[xWheelColliders.Length - 2].motorTorque = 0;
                xWheelColliders[xWheelColliders.Length - 2].brakeTorque = iBrakeTorque;
                xWheelColliders[xWheelColliders.Length - 1].motorTorque = 0;
                xWheelColliders[xWheelColliders.Length - 1].brakeTorque = iBrakeTorque;
            }
        }

        // If the vehicle is stationary
        if (fEngineRPM > 0 && Input.GetAxis("Vertical") < 0 && fEngineRPM <= fGearUpRPM)
        {
            // Apply the braking force
            xWheelColliders[0].brakeTorque = iBrakeTorque;
            xWheelColliders[1].brakeTorque = iBrakeTorque;
        }
        else
        {
            // Otherwise, reset the braking force
            xWheelColliders[0].brakeTorque = 0;
            xWheelColliders[1].brakeTorque = 0;
        }
    }

    // Function to apply steering
    void ApplySteering()
    {
        // If the current speed is lower than the high speed threshold
        if (fCurrentSpeed < iSteerAngleHighSpeedThreshold)
        {
            // Apply sharper turning
            fSteerAngle = iSteerAngleLowSpeed - (fCurrentSpeed / 10);
        }
        else
        {
            // Otherwise, Apply less sharp turning
            fSteerAngle = iSteerAngleHighSpeed;
        }
        // Apply the steering angle to the front 2 wheels
        xWheelColliders[0].steerAngle = fSteerAngle * Input.GetAxis("Horizontal");
        xWheelColliders[1].steerAngle = fSteerAngle * Input.GetAxis("Horizontal");
    }

    // Function to apply manual transmission
    void ManualGearTransmission()
    {
        // If the current gear is within bounds
        if (iCurrentGear >= 0 && iCurrentGear < fGearRatios.Length - 1)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                // Gear up
                iCurrentGear = iCurrentGear + 1;
            }
        }
        // If the current gear is within bounds
        if (iCurrentGear >= 0 && iCurrentGear <= fGearRatios.Length - 1)
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                // Gear down
                iCurrentGear = iCurrentGear - 1;
            }
        }
        // Safety Check to prevent negative gears
        if (iCurrentGear == -1)
        {
            iCurrentGear = 0;
        }

    }

    // Function to apply automatic transmission
    void AutomaticGearTransmission()
    {
        // Set appropriate gear
        AppropriateGear = iCurrentGear;
        // If the engine RPM is higher than the Gear Up RPM
        if (fEngineRPM >= fGearUpRPM)
        {
            // Loop through the gear ratios
            for (int i = 0; i < fGearRatios.Length; i++)
            {
                // If the RPM is less than the gear up RPM
                if (xWheelColliders[Random.Range(0, xWheelColliders.Length)].rpm * fGearRatios[i] < fGearUpRPM)
                {
                    // Set the appropriate gear
                    AppropriateGear = i;
                    break;
                }
            }
            // Set the current gear
            iCurrentGear = AppropriateGear;
        }

        // If the engine RPM is lower than the Gear Up RPM
        if (fEngineRPM <= fGearDownRPM)
        {
            // Set appropriate gear
            AppropriateGear = iCurrentGear;
            // Loop through the gear ratios backwards
            for (int j = fGearRatios.Length - 1; j >= 0; j--)
            {
                // If the RPM is greater than the gear down RPM
                if (xWheelColliders[Random.Range(0, xWheelColliders.Length)].rpm * fGearRatios[j] > fGearDownRPM)
                {
                    // Set the appropriate gear
                    AppropriateGear = j;
                    break;
                }
            }
            // Set the current gear
            iCurrentGear = AppropriateGear;
        }
    }

    // Function to apply a hand-braking force
    void ApplyHandBrakes()
    {
        // Loop through all the wheels
        for (int i = 0; i < xWheelColliders.Length; i++)
        {
            // Apply the brake force
            xWheelColliders[i].brakeTorque = iBrakeTorque;
        }
    }
}