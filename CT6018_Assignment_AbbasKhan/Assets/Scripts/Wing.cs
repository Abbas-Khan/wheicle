﻿///////////////////////////////////////////////////////////////////
// Name: Wing.cs
// Author: Abbas Khan
// Date: 23/09/18
// Bio: Used to control the Wing of the vehicle
///////////////////////////////////////////////////////////////////

// Using Calls
using UnityEngine;

public class Wing : MonoBehaviour
{
    // Rigidbody variable to hold the local rigidbody
    Rigidbody xRigidBody;
    // Float for the lift coefficient
    public float fLiftCoefficient = 10;
    // Float to hold the lift frorce
    private float fLift;

    void Start()
    {
        // Assign the rigidbody
        xRigidBody = GetComponent<Rigidbody>();
    }

    void Update()
    {
        // Safety null check
        if (xRigidBody != null)
        {
            // Float to hold the lift frorce
            float fLift = fLiftCoefficient * xRigidBody.velocity.sqrMagnitude;
            // Apply the force to the rigidbody
            xRigidBody.AddForceAtPosition(fLift * -transform.up, transform.position);
        }
    }
}
